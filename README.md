# nuxt-mazyway

> My glorious Nuxt.js project

## Build Setup

``` bash
# install dependencies
$ npm run install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).

helps link:

- [blog-reveal.nuxt](https://github.com/patarapolw/blog-reveal.nuxt/blob/master/utils/build-api.ts)
- [Nuxt-Template](https://github.com/shaomingwei/Nuxt-Template/blob/master/package.json)
- [Anyushu_Nuxt](https://github.com/Anyushu/Anyushu_Nuxt)
- [jo0ger API](https://github.com/jo0ger/jooger.me)
- [todo-api-rails-front-nuxtjs-sample](https://github.com/greendrop/todo-api-rails-front-nuxtjs-sample)

