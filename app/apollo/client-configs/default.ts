import { InMemoryCache, IntrospectionFragmentMatcher } from 'apollo-cache-inmemory';
/**
 * fragments.json is generated from the GQL server at runtime
 */
import schema from './fragments.json';

const fragmentMatcher = new IntrospectionFragmentMatcher({
  introspectionQueryResultData: schema,
});

export default (): object => {
  const headers = {
    'Accept-Language': 'en-us',
  };
  return {
    httpEndpoint: process.env.GQL_URL,
    httpLinkOptions: { headers },
    cache: new InMemoryCache({ fragmentMatcher }),
  };
};
