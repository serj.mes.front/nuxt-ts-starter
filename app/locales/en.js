export default {
  labels: {
    listModel: 'listModel',
    showModel: 'showModel',
    newModel: 'newModel',
    editModel: 'editModel',
  },
};
