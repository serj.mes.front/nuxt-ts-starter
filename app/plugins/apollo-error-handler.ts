// eslint-disable-next-line @typescript-eslint/no-explicit-any
export default (error: any, context: any): void => {
  console.log(error);
  context.error({ statusCode: 304, message: 'Server error' });
};
