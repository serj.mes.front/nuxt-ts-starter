import { Getters as G } from 'vuex-smart-module';
import { State as S } from './state';

export class Getters extends G<S> {
  public get getCount(): number {
    return this.state.count;
  }
}
