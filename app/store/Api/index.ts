import { Module } from 'vuex-smart-module';
import { State as state } from './state';
import { Actions as actions } from './actions';
import { Mutations as mutations } from './mutations';
import { Getters as getters } from './getters';

export default new Module({ state, actions, mutations, getters });
