import { Mutations as M } from 'vuex-smart-module';
import { State } from './state';

export class Mutations extends M<State> {
  reset(): void {
    const s = new State();
    Object.keys(s).forEach(key => {
      // this.state[key] = s[key];
    });
  }

  setNewCount(payload: number): void {
    this.state.count += payload;
  }
}
