import { Getters, Mutations, Actions, Module } from 'vuex-smart-module';

class State {
  count = 1;
}

class FooGetters extends Getters<State> {
  get double(): number {
    return this.state.count * 2;
  }

  get triple(): number {
    return this.getters.double + this.state.count;
  }
}

class FooMutations extends Mutations<State> {
  increment(payload: number): void {
    this.state.count += payload;
  }
}

class FooActions extends Actions<State, FooGetters, FooMutations, FooActions> {
  incrementAsync(payload: { amount: number; interval: number }): object {
    return new Promise(() => {
      setTimeout(() => {
        this.commit('increment', payload.amount);
      }, payload.interval);
    });
  }
}

export default new Module({
  state: State,
  getters: FooGetters,
  mutations: FooMutations,
  actions: FooActions,
});
