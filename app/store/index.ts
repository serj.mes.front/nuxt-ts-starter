/**
 * Doc: https://habr.com/ru/post/459050/
 */
import { createStore, Module } from 'vuex-smart-module';
import Api from './Api';
import Directions from './Directions';

export const modules = {
  Api,
  Directions,
};

export const store = createStore(new Module({ modules }), {
  strict: process.env.NODE_ENV !== 'production',
});
