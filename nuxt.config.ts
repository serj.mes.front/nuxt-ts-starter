import { Configuration } from '@nuxt/types';

const isDev = process.env.NODE_ENV === 'development';

const config: Configuration = {
  /*
  |------------------------------------------------------
  | Mode
  |------------------------------------------------------
  */
  mode: 'universal',

  /*
  |------------------------------------------------------
  | Extensions
  |------------------------------------------------------
  */
  extensions: ['ts', 'js'],

  /*
  |------------------------------------------------------
  | Transition
  |------------------------------------------------------
  */
  pageTransition: { name: 'layout' },

  /*
  |------------------------------------------------------
  | Path
  |------------------------------------------------------
  */
  srcDir: 'app/',

  /*
  |------------------------------------------------------
  | Headers
  |------------------------------------------------------
  */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },

  /*
  |------------------------------------------------------
  | Progress-bar
  |------------------------------------------------------
  */
  loading: { color: '#fff' },

  /*
  |------------------------------------------------------
  | Global CSS
  |------------------------------------------------------
  */
  css: ['element-ui/lib/theme-chalk/index.css', '~/assets/styles/main.scss'],

  /*
  |------------------------------------------------------
  | Router
  |------------------------------------------------------
  */
  router: {
    linkExactActiveClass: 'active',
  },

  /*
  |------------------------------------------------------
  | Plugins
  |------------------------------------------------------
  */
  plugins: ['~/plugins/element-ui', '~/plugins/i18n.ts', { src: '~/plugins/inject-ww', ssr: false }],

  /*
  |------------------------------------------------------
  | Build Modules
  |------------------------------------------------------
  */
  buildModules: ['@nuxtjs/eslint-module', '@nuxtjs/stylelint-module', '@nuxt/typescript-build'],

  /*
  |------------------------------------------------------
  | Modules
  |------------------------------------------------------
  */
  modules: [
    'nuxt-i18n',
    '@nuxtjs/axios',
    '@nuxtjs/dotenv',
    '@nuxtjs/apollo',
    '@nuxtjs/style-resources',
    [
      'nuxt-mq',
      {
        defaultBreakpoint: 'default',
        breakpoints: {
          sm: 450,
          md: 1250,
          lg: Infinity,
        },
      },
    ],
    [
      'nuxt-i18n',
      {
        locales: [
          {
            code: 'en',
            iso: 'en-US',
            file: 'en.js',
            name: 'English',
          },
          {
            code: 'ru',
            iso: 'ru-RU',
            file: 'ru.js',
            name: 'Русский',
          },
        ],
        defaultLocale: 'ru',
        lazy: true,
        langDir: '~/locales/',
      },
    ],
  ],

  /*
  |------------------------------------------------------
  | Axios module configuration
  |------------------------------------------------------
  */
  axios: {
    debug: isDev,
  },

  /*
  |------------------------------------------------------
  | Apollo
  |------------------------------------------------------
  */
  apollo: {
    tokenName: 'apollo-token',
    tokenExpires: 5,
    includeNodeModules: true,
    authenticationType: 'Basic',
    clientConfigs: {
      default: '~/apollo/client-configs/default.ts',
    },
    errorHandler: '~/plugins/apollo-error-handler.ts',
  },

  /*
  |------------------------------------------------------
  | Sitemap
  |------------------------------------------------------
  */
  sitemap: {
    path: '/sitemap.xml',
    hostname: 'http://localhost:3000',
    generate: true,
    exclude: [],
  },

  /*
  |------------------------------------------------------
  | Generate
  |------------------------------------------------------
  */
  generate: {
    dir: 'public',
    fallback: true, // '404.html'
  },

  /*
  |------------------------------------------------------
  | Build
  |------------------------------------------------------
  */
  build: {
    transpile: [/^element-ui/],
    hotMiddleware: {
      client: {
        overlay: false,
      },
    },
    publicPath: '/assets/',
    extractCSS: true,
    terser: {
      terserOptions: {
        // eslint-disable-next-line @typescript-eslint/camelcase
        compress: { drop_console: process.env.NODE_ENV === 'production' },
      },
    },
    babel: {
      plugins: [
        ['@babel/plugin-proposal-decorators', { legacy: true }],
        ['@babel/plugin-proposal-class-properties', { loose: true }],
      ],
    },
  },

  /*
  |------------------------------------------------------
  | Typescript
  |------------------------------------------------------
  */
  typescript: {
    typeCheck: {
      eslint: true,
    },
  },
};

export default config;
